# OpenML dataset: CF-metadataset-auc

https://www.openml.org/d/41073

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Tiago Cunha, Carlos Soares, Andre C.P.L.F. de Carvalho  
**Source**: Unknown - Date unknown  
**Please cite**: Cunha, T., Soares, C., and de Carvalho, A. C. P. L. F. (2016). Selecting Collaborative Filtering algorithms using Metalearning. In European Conference on Machine Learning and Knowledge Discovery in Databases (pp. 393&ndash;409).  

Collaborative Filtering Metalearning problem: datasets metafeatures associated with the best algorithms using AUC evaluation measure.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41073) of an [OpenML dataset](https://www.openml.org/d/41073). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41073/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41073/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41073/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

